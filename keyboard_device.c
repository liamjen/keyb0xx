#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>

#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

#include "keyboard_device.h"

void print_dev_info(struct libevdev *dev) {
   printf("Input device name: \"%s\"\n", libevdev_get_name(dev));
   printf("Input device ID: bus %#x vendor %#x product %#x\n",
	  libevdev_get_id_bustype(dev),
	  libevdev_get_id_vendor(dev),
	  libevdev_get_id_product(dev));
   printf("uniq: %s\n", libevdev_get_uniq(dev));
}


char* get_keyboard_device_path() {
   const int max_devices = 80;

   char *dev_path = "/dev/input/";
   const char *device_tag = "event";
   const char *kb_device_name_tag = "Keyboard";

   DIR *d = opendir(dev_path);
   struct dirent *dir;

   // Holds EVERY event file in /dev/input/
   char* device_paths[max_devices];
   char* device_names[max_devices];

   // Holds event files with the name 'Keyboard/
   char* keyboard_paths[max_devices];
   char* keyboard_names[max_devices];

   int num_devices_found = 0;
   int num_keyboards_found = 0;

   if(d) {
	   while((dir = readdir(d)) != NULL) {
		   if(strstr(dir->d_name, device_tag)) {
   			   char* path;
			   path = (char*)malloc(strlen(dev_path) + strlen(dir->d_name) + 1);
			   strcpy(path, dev_path);
			   strcat(path, dir->d_name);

			   struct libevdev *dev = NULL;
			   libevdev_new_from_fd(open(path, O_RDONLY), &dev);
			   device_paths[num_devices_found] = path;
			   char* name = (char*)libevdev_get_name(dev);
			   device_names[num_devices_found] = name;
			   num_devices_found++;

			   // Detected a keyboard device
			   if(strstr(name, kb_device_name_tag)) {
			      keyboard_paths[num_keyboards_found] = path;
			      keyboard_names[num_keyboards_found] = name;
			      num_keyboards_found++;
			   }
		   }
	   }
	   closedir(d);
   }
   int max_input_length = 3; // Who has over 100 keyboards??
   char in[max_input_length];

   if(num_keyboards_found) {
      printf("\n\nMultiple keyboards found... Which keyboard would you like to use?\n");
      for(int i = 0; i < num_keyboards_found; i++)
	  printf("[%d] %s\n", i, keyboard_names[i]);
      printf("Enter a number (or 'a' to show all devices): ");
      fflush(stdout);
      fgets(in, max_input_length, stdin);
      int choice = atoi(in);
      if(choice >= num_keyboards_found) {
	 fprintf(stderr, "Device out of range\n");
	 exit(1);
      } else if (in[0] != 'a')
      	return keyboard_paths[choice];
   } 
   if(num_devices_found) {
      printf("\n\nMultiple devices found... Which device would you like to use?\n");
      for(int i = 0; i < num_devices_found; i++)
	  printf("[%d] %s\n", i, device_names[i]);
      printf("Enter a number: ");
      fflush(stdout);
      fgets(in, max_input_length, stdin);
      int choice = atoi(in);
      if(choice >= num_devices_found) {
	 fprintf(stderr, "Keyboard out of range\n");
	 exit(1);
      }
      return device_paths[choice];
   } else {
      fprintf(stderr, "No devices found");
      exit(1);
   }
}

struct libevdev* get_keyboard_device() {
   struct libevdev *dev = NULL;
   int fd;
   int rc = 1;
   
   char* keyboard_path = get_keyboard_device_path();

   fd = open(keyboard_path, O_RDONLY);

   if(fd < 0) {
      printf("File error opening file device: %s", keyboard_path);
      exit(1);
   } else {
      printf("Using device: %s\n", keyboard_path);
   }
   rc = libevdev_new_from_fd(fd, &dev);
   if (rc < 0) {
	   fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
	   exit(1);
   }
   return dev;
}
